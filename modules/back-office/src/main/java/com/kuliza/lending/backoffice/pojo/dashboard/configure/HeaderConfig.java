package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.Set;

public class HeaderConfig {

	private String label;
	private String key;
	private Set<String> variables;

	public HeaderConfig() {
		super();
	}

	public HeaderConfig(String label, String key, Set<String> variables) {
		super();
		this.label = label;
		this.key = key;
		this.variables = variables;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Set<String> getVariables() {
		return variables;
	}

	public void setVariables(Set<String> variables) {
		this.variables = variables;
	}

}
