package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kuliza.lending.backoffice.models.Cards;
import com.kuliza.lending.backoffice.models.CardsVariablesMapping;
import com.kuliza.lending.backoffice.models.Headers;
import com.kuliza.lending.backoffice.utils.BackOfficeConstants;

public class CardsConfig {

	private String label;
	private List<HeadersConfig> headers;
	private List<VariablesConfig> variables;

	public CardsConfig() {
		super();
	}

	public CardsConfig(String label, List<HeadersConfig> headers, List<VariablesConfig> variables) {
		super();
		this.label = label;
		this.headers = headers;
		this.variables = variables;
	}

	public CardsConfig(Cards card, Map<String, Object> data) {
		super();
		this.label = card.getLabel();
		this.headers = new ArrayList<>();
		this.variables = new ArrayList<>();
		Set<Headers> cardHeaders = card.getHeaders();
		for (Headers header : cardHeaders) {
			headers.add(new HeadersConfig(header, header.getHeadersVariables(), data));
		}
		Set<CardsVariablesMapping> cvmappings = card.getCardsVariables();
		for (CardsVariablesMapping cvmap : cvmappings) {
			if (cvmap.getVariable().getType().equalsIgnoreCase(BackOfficeConstants.VAR_TYPE_DROPDOWN)) {
				variables.add(
						new VariablesOptionListConfig(cvmap.getVariable(), data.get(cvmap.getVariable().getMapping())));
			} else {
				variables.add(new VariablesConfig(cvmap.getVariable(), data.get(cvmap.getVariable().getMapping())));
			}
		}

	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public List<HeadersConfig> getHeaders() {
		return headers;
	}

	public void setHeaders(List<HeadersConfig> headers) {
		this.headers = headers;
	}

	public List<VariablesConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<VariablesConfig> variables) {
		this.variables = variables;
	}

}
