package com.kuliza.lending.backoffice.utils;

import java.util.HashMap;
import java.util.Map;

public class BackOfficeConstants {

	public static final String STATUS_VARIABLE_KEY = "status";
	public static final String STATUS_MESSAGE_VARIABLE_KEY = "statusMessage";
	public static final String HISTORY_BUCKET_KEY = "history";

	public static final String FILTER_OFFSET_KEY = "offset";
	public static final String FILTER_PAGE_WIDTH_KEY = "pageWidth";

	private static final String INVALID_LOS_APPLICATION_ID_MESSAGE = "Invalid LOS Application Id : ";
	private static final String USER_NOT_INVOLVED_IN_APPLICATION_MESSAGE = "User not involved in this application id : ";
	private static final String DASHBOARD_NOT_CONFIGURED_FOR_ROLE_MESSAGE = "Dashboard not configured for role : ";
	private static final String COMMENT_NOT_ENABLED_FOR_ROLE_MESSAGE = "Comment not enabled for role : ";
	private static final String TAB_NOT_CONFIGURED_FOR_ROLE_MESSAGE = "Tab not configured for role : ";
	private static final String BUCKET_NOT_CONFIGURED_FOR_ROLE_MESSAGE = "Bucket not configured for role : ";
	private static final String VARIABLE_NOT_MAPPED_FOR_ROLE_MESSAGE = "Variable not mapped for role : ";
	private static final String INVALID_OUTCOME_MESSAGE = "Invalid Outcome : ";

	public static final String ERROR_OPTION_FOR_DROPDOWN = "ERROR_BO_DROPDOWN_KEY_01";
	public static final String ERROR_FOR_NUMBER = "ERROR_BO_NUMBER_FIELD_02";
	public static final String ERROR_FOR_DATE = "ERROR_BO_DATE_FIELD_03";
	public static final String ERROR_FOR_UPLOAD = "ERROR_BO_UPLOAD_FIELD_04";

	public static final String VAR_TYPE_STRING = "text";
	public static final String VAR_TYPE_NUMBER = "number";
	public static final String VAR_TYPE_DROPDOWN = "dropdown";
	public static final String VAR_TYPE_UPLOAD = "upload";
	public static final String JOURNEY_TYPE = "journeyType";

	public static String getInvalidLOSApplicationIdMessage(String losApplicationId) {
		return INVALID_LOS_APPLICATION_ID_MESSAGE + losApplicationId;
	}

	public static String getUserNotInvolvedInApplicationMessage(String losApplicationId) {
		return USER_NOT_INVOLVED_IN_APPLICATION_MESSAGE + losApplicationId;
	}

	public static String getDashboardNotConfiguredForRoleMessage(String role) {
		return DASHBOARD_NOT_CONFIGURED_FOR_ROLE_MESSAGE + role;
	}

	public static String getTabNotConfiguredForRoleMessage(String role) {
		return TAB_NOT_CONFIGURED_FOR_ROLE_MESSAGE + role;
	}

	public static String getBucketNotConfiguredForRoleMessage(String role) {
		return BUCKET_NOT_CONFIGURED_FOR_ROLE_MESSAGE + role;
	}

	public static String getVariableNotMappedForRoleMessage(String role) {
		return VARIABLE_NOT_MAPPED_FOR_ROLE_MESSAGE + role;
	}

	public static String getInvalidOutcomeMessage(String outcome) {
		return INVALID_OUTCOME_MESSAGE + outcome;
	}

	public static String getCommentNotEnabledMessage(String role) {
		return COMMENT_NOT_ENABLED_FOR_ROLE_MESSAGE + role;
	}


	public static Map<String, String> JOURNEY_TYPE() {
		/*
		 * Kindly change your Journey Type Label with Journey Type Key
		 */
		final Map<String, String> journeyMap = new HashMap<>();
		journeyMap.put("SCB_PoC", "Business Loan");
		journeyMap.put("Auto_Loan_SCB", "Auto Loan");
		return journeyMap;

	}
}
