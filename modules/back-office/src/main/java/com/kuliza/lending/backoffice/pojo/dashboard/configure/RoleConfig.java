package com.kuliza.lending.backoffice.pojo.dashboard.configure;

import java.util.List;

public class RoleConfig {

	private String roleName;
	private boolean enableComments;
	private List<VariableConfig> variables;
	private List<BucketConfig> buckets;
	private List<TabConfig> tabs;
	private List<OutcomeConfig> outcomes;

	public RoleConfig() {
		super();
	}

	public RoleConfig(String roleName, boolean enableComments, List<VariableConfig> variables,
			List<BucketConfig> buckets, List<TabConfig> tabs, List<OutcomeConfig> outcomes) {
		super();
		this.roleName = roleName;
		this.enableComments = enableComments;
		this.variables = variables;
		this.buckets = buckets;
		this.tabs = tabs;
		this.outcomes = outcomes;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<VariableConfig> getVariables() {
		return variables;
	}

	public void setVariables(List<VariableConfig> variables) {
		this.variables = variables;
	}

	public List<BucketConfig> getBuckets() {
		return buckets;
	}

	public void setBuckets(List<BucketConfig> buckets) {
		this.buckets = buckets;
	}

	public List<TabConfig> getTabs() {
		return tabs;
	}

	public void setTabs(List<TabConfig> tabs) {
		this.tabs = tabs;
	}

	public List<OutcomeConfig> getOutcomes() {
		return outcomes;
	}

	public void setOutcomes(List<OutcomeConfig> outcomes) {
		this.outcomes = outcomes;
	}

	public boolean isEnableComments() {
		return enableComments;
	}

	public void setEnableComments(boolean enableComments) {
		this.enableComments = enableComments;
	}

}
