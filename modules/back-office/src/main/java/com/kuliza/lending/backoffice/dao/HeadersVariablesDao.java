package com.kuliza.lending.backoffice.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Headers;
import com.kuliza.lending.backoffice.models.HeadersVariablesMapping;
import com.kuliza.lending.backoffice.models.Variables;

@Repository
public interface HeadersVariablesDao extends CrudRepository<HeadersVariablesMapping, Long> {

	public HeadersVariablesMapping findByHeaderAndVariableAndIsDeleted(Headers header, Variables variables,
			boolean isDeleted);

}
