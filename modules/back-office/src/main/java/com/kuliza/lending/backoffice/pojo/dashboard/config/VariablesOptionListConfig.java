package com.kuliza.lending.backoffice.pojo.dashboard.config;

import java.util.ArrayList;
import java.util.List;

import com.kuliza.lending.backoffice.models.Options;
import com.kuliza.lending.backoffice.models.Variables;

public class VariablesOptionListConfig extends VariablesConfig {

	private List<OptionsConfig> options;

	public List<OptionsConfig> getOptions() {
		return options;
	}

	public void setOptions(List<OptionsConfig> options) {
		this.options = options;
	}

	public VariablesOptionListConfig(List<OptionsConfig> options) {
		super();
		this.options = options;
	}

	public VariablesOptionListConfig() {
		super();
	}

	public VariablesOptionListConfig(String label, String key, boolean editable, boolean writable, String meta,
			String type) {
		super(label, key, editable, writable, meta, type);
	}

	public VariablesOptionListConfig(Variables variable, Object value) {
		super(variable.getLabel(), variable.getMapping(), variable.isEditable(), variable.isWritable(),
				variable.getMeta(), variable.getType());
		List<OptionsConfig> optionList = new ArrayList<>();
		this.setValue(value);
		for (Options option : variable.getOptions()) {
			OptionsConfig optn = new OptionsConfig(option.getLabel(), option.getOptionKey());
			optionList.add(optn);
		}
		this.options = optionList;
	}

}
