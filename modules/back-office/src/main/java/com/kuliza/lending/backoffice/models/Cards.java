package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_cards", uniqueConstraints = { @UniqueConstraint(columnNames = { "cardsKey", "tab_id" }) })
public class Cards extends BaseModel {

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String cardsKey;

	@Column(nullable = false)
	private int cardsOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "tab_id")
	private Tabs tab;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "card", orphanRemoval = true)
	@OrderBy("headerOrder ASC")
	private Set<Headers> headers;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "card", orphanRemoval = true)
	@OrderBy("cardVariableOrder ASC")
	private Set<CardsVariablesMapping> cardsVariables;

	public Cards() {
		super();
		this.setIsDeleted(false);
	}

	public Cards(String label, String cardsKey, int cardsOrder, Tabs tab) {
		super();
		this.label = label;
		this.cardsKey = cardsKey;
		this.cardsOrder = cardsOrder;
		this.tab = tab;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getCardsKey() {
		return cardsKey;
	}

	public void setCardsKey(String cardsKey) {
		this.cardsKey = cardsKey;
	}

	public int getCardsOrder() {
		return cardsOrder;
	}

	public void setCardsOrder(int cardsOrder) {
		this.cardsOrder = cardsOrder;
	}

	public Tabs getTab() {
		return tab;
	}

	public void setTab(Tabs tab) {
		this.tab = tab;
	}

	public Set<CardsVariablesMapping> getCardsVariables() {
		return cardsVariables;
	}

	public Set<Headers> getHeaders() {
		return headers;
	}

}
