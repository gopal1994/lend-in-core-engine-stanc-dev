package com.kuliza.lending.journey.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("LmsDemoService")
public class LmsDemoService {

    private RuntimeService runtimeService;

    private String baseUrl;
    private Map<String, String> defaultHeaders;
    private String defaultDateFormat;
    private String defaultLocale;
    private int defaultOfficeId;
    @Autowired
    public LmsDemoService(RuntimeService runtimeService){
        this.runtimeService = runtimeService;
        this.initDefault();
    }

    private void initDefault(){
        this.defaultHeaders = new HashMap<>();
        defaultHeaders.put("Authorization", "Basic bWlmb3M6cGFzc3dvcmQ=");
        defaultHeaders.put("Content-Type", "application/json");
        defaultHeaders.put("Fineract-Platform-TenantId", "default");

        this.baseUrl = "https://lms-demo.getlend.in/fineract-provider/api/v1/";
        this.defaultDateFormat = "dd MMMM yyyy";
        this.defaultLocale = "en";
        this.defaultOfficeId = 2;
    }

    public DelegateExecution createClientApiIntegration(DelegateExecution execution) throws Exception{



        String rootProcessInstanceId = execution.getRootProcessInstanceId();


        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("address", new JSONArray());
        requestBody.put("familyMembers", new JSONArray());
        requestBody.put("officeId", this.defaultOfficeId);
        requestBody.put("firstname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "firstName")));
        requestBody.put("lastname", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "lastName")));
        requestBody.put("genderId", 22);
        requestBody.put("active", false);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("activationDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("savingsProductId", null);

        String clientId;
        try{

            System.out.println("request body createClientApiIntegration: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+ "clients")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("createClientApiIntegration-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("createClientApiIntegration api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            clientId = body.optString("clientId");

            runtimeService.setVariable(rootProcessInstanceId, "clientIdFromLMS", clientId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }


        this.createDatatableForClient(execution, clientId);
        this.createDatatableDataForClient(execution, clientId);
        this.activateClient(execution, clientId);

        return null;
    }



    private void createDatatableForClient(DelegateExecution execution, String cId) throws Exception{

        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("first", "Active");
        requestBody.put("second", "1");
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);

        try{

            HttpResponse<String> response = Unirest.post("https://lms-demo.getlend.in/fineract-provider/api/v1/datatables/one/"
                    + String.valueOf(cId))
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("createDatatableForClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    private void createDatatableDataForClient(DelegateExecution execution, String cId) throws Exception{


        String rootProcessInstanceId = execution.getRootProcessInstanceId();
        Map<String, Object> processVariables = runtimeService.getVariables(rootProcessInstanceId);
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("Employment Type", String.valueOf(processVariables.getOrDefault("typeOfEmployment", "")));
        requestBody.put("Aadhaar VID", String.valueOf(processVariables.getOrDefault("aadhaarVid", "")));
        requestBody.put("Address line1", String.valueOf(processVariables.getOrDefault("enterCurrentAddress", "")));
        requestBody.put("Address line2", String.valueOf(processVariables.getOrDefault("enterCurrentAddress2", "")));
        requestBody.put("Pincode", String.valueOf(processVariables.getOrDefault("pincode2", "")));
        requestBody.put("City", String.valueOf(processVariables.getOrDefault("city2", "")));
        requestBody.put("State", String.valueOf(processVariables.getOrDefault("state2", "")));
        requestBody.put("Purpose of loan", String.valueOf(processVariables.getOrDefault("purposeOfLoan", "")));
        requestBody.put("Monthly Income", String.valueOf(processVariables.getOrDefault("monthlyIncome", "")));
        requestBody.put("Existing EMI", String.valueOf(processVariables.getOrDefault("emi", "")));
        requestBody.put("PAN", String.valueOf(processVariables.getOrDefault("pan", "")));
        requestBody.put("Designation", String.valueOf(processVariables.getOrDefault("designation", "")));
        requestBody.put("Experience", String.valueOf(processVariables.getOrDefault("experience", "")));
        requestBody.put("Official Email id", String.valueOf(processVariables.getOrDefault("officialEmailId", "")));


        String officeAddress = String.valueOf(processVariables.getOrDefault("enterOfficeAddress", "")) + " "+
                String.valueOf(processVariables.getOrDefault("enterOfficeAddress2", "")) + " "+
                String.valueOf(processVariables.getOrDefault("city3", "")) + " "+
                String.valueOf(processVariables.getOrDefault("state3", "")) + " "+
                String.valueOf(processVariables.getOrDefault("pincode3", ""));

        requestBody.put("Office Address", officeAddress);
        requestBody.put("Bank Account Number", String.valueOf(processVariables.getOrDefault("bankAccountNumber", "")));
        requestBody.put("IFSC Code", String.valueOf(processVariables.getOrDefault("ifscCode", "")));
        requestBody.put("Bank Name", String.valueOf(processVariables.getOrDefault("salaryBank", "")));
        requestBody.put("Branch", String.valueOf(processVariables.getOrDefault("salaryBranch", "")));
        requestBody.put("Mode of repayment", String.valueOf(processVariables.getOrDefault("aadhaarVid", "")));
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);


        try{
            System.out.println("request body createDatatableDataForClient: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"datatables/Personal Loan Additional Information 1/"
                    + String.valueOf(cId))
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("createDatatableDataForClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }


    private void activateClient(DelegateExecution execution, String cId) throws Exception{


        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("activationDate",  convertDate(new Date(), defaultDateFormat));
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);

        try{
            System.out.println("request body activateClient: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"clients/"
                    + cId + "?command=activate")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("activateClient-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("activateClient api issue != 200");
            JSONObject body = new JSONObject(response.getBody());

        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    public DelegateExecution createLoanApiIntegration(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();


        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("clientId", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "clientIdFromLMS")));
        requestBody.put("productId", 18);
        requestBody.put("disbursementData", new JSONArray());
        requestBody.put("principal",  String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount")));
        requestBody.put("loanTermFrequency", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("numberOfRepayments", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("interestRatePerPeriod", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "roi")));
        requestBody.put("amortizationType", 1);
        requestBody.put("isEqualAmortization", false);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("allowPartialPeriodInterestCalcualtion", true);
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("loanType", "individual");
        requestBody.put("expectedDisbursementDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));


        try{
            System.out.println("request body LMS: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"loans")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("LMS-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("LMS api issue != 200");
            JSONObject body = new JSONObject(response.getBody());
            String loanId = body.optString("loanId");

            runtimeService.setVariable(rootProcessInstanceId, "loanIdFromLMS", loanId);


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }

        this.createRepaymentSchedule(execution);
        return execution;

    }


    private void createRepaymentSchedule(DelegateExecution execution) throws Exception{

        String rootProcessInstanceId = execution.getRootProcessInstanceId();

        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("locale", defaultLocale);
        requestBody.put("dateFormat", this.defaultDateFormat);
        requestBody.put("productId", 18);
        requestBody.put("principal", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "loanAmount")));
        requestBody.put("loanTermFrequency", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("loanTermFrequencyType", 2);
        requestBody.put("numberOfRepayments", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "tenure")));
        requestBody.put("repaymentEvery", 1);
        requestBody.put("repaymentFrequencyType", 2);
        requestBody.put("interestRatePerPeriod", String.valueOf(runtimeService.getVariable(rootProcessInstanceId,
                "roi")));
        requestBody.put("amortizationType", 1);
        requestBody.put("interestType", 0);
        requestBody.put("interestCalculationPeriodType", 1);
        requestBody.put("expectedDisbursementDate", convertDate(new Date(), defaultDateFormat));
        requestBody.put("transactionProcessingStrategyId", 1);
        requestBody.put("submittedOnDate", convertDate(new Date(), this.defaultDateFormat));
        requestBody.put("loanType", "individual");
        requestBody.put("clientId", String.valueOf(runtimeService.getVariable(rootProcessInstanceId, "clientIdFromLMS")));

        try{
            System.out.println("request body  createRepaymentSchedule: " + new JSONObject(requestBody).toString());
            HttpResponse<String> response = Unirest.post(this.baseUrl+"loans?command=calculateLoanSchedule")
                    .headers(defaultHeaders)
                    .body(new JSONObject(requestBody)).asString();
            System.out.println("createRepaymentSchedule-RESPONSE : " + response.getBody());

            if (response.getStatus() != 200)
                throw new Exception("createRepaymentSchedule api issue != 200");


            runtimeService.setVariable(rootProcessInstanceId, "repaymentScheduleFromLMS", response.getBody());


        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }


    private static String convertDate(Date date, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }


}
