package com.kuliza.lending.journey.service;

import java.util.*;

import org.apache.commons.codec.binary.Base64;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.*;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.authorization.service.KeyCloakManager;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.service.GenericServices;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.Enums;
import com.kuliza.lending.common.utils.Enums.APPLICATION_STAGE;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.common.utils.LogType;
import com.kuliza.lending.journey.model.LOSApplicationNumber;
import com.kuliza.lending.journey.model.LOSApplicationNumberDao;
import com.kuliza.lending.journey.model.LOSUserAddressDao;
import com.kuliza.lending.journey.model.LOSUserAddressModel;
import com.kuliza.lending.journey.model.LOSUserDao;
import com.kuliza.lending.journey.model.LOSUserEmploymentDao;
import com.kuliza.lending.journey.model.LOSUserEmploymentModel;
import com.kuliza.lending.journey.model.LOSUserLoanDataDao;
import com.kuliza.lending.journey.model.LOSUserLoanDataModel;
import com.kuliza.lending.journey.model.LOSUserModel;
import com.kuliza.lending.journey.pojo.BackTaskInput;
import com.kuliza.lending.journey.pojo.CustomerLoginInput;
import com.kuliza.lending.journey.pojo.SubmitFormClass;
import com.kuliza.lending.journey.utils.HelperFunctions;
import com.kuliza.lending.journey.utils.Validation;

@Service
public class UserJourneyServices extends GenericServices {

	private static final Logger logger = LoggerFactory.getLogger(UserJourneyServices.class);

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private IdentityService identityService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private LOSApplicationNumberDao losApplicationNumberDao;

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private LOSUserDao losUserDao;

	@Autowired
	private KeyCloakManager keycloakManager;

	@Autowired
	private LOSUserLoanDataDao losUserLoanDataDao;

	@Autowired
	private LOSUserAddressDao losUserAddressDao;

	@Autowired
	private LOSUserEmploymentDao losUserEmploymentDao;

	/**
	 * This functions takes Parent/Root Process Instance Id and returns List of All
	 * Process Instance Ids of Running Child Executions
	 * 
	 * @param parentProcessInstanceId
	 * @return List of String Process Instance Ids
	 * 
	 * @author Arpit Agrawal
	 */
	public List<String> allProcessInstances(String parentProcessInstanceId) {
		List<Execution> allRunningExecutions = runtimeService.createExecutionQuery()
				.rootProcessInstanceId(parentProcessInstanceId).list();
		List<String> allProcessInstanceIds = new ArrayList<>();
		for (Execution singleExecution : allRunningExecutions) {
			allProcessInstanceIds.add(singleExecution.getProcessInstanceId());
		}
		allProcessInstanceIds.add(parentProcessInstanceId);
		return allProcessInstanceIds;
	}

	/**
	 * This functions return the current task data For the given user associated
	 * with given process instance id.
	 * 
	 * @param userId
	 * @param parentProcessInstanceId
	 * @param errorcode
	 * @param errorMessage
	 * @return ApiResponse
	 * @throws Exception
	 * 
	 * @author Arpit Agrawal
	 */
	@SuppressWarnings("unchecked")
	public ApiResponse getTaskData(String userId, String parentProcessInstanceId, HttpStatus errorcode,
			String errorMessage) throws Exception {
		ApiResponse response;
		List<String> allProcessInstanceIds = allProcessInstances(parentProcessInstanceId);

		List<Task> allTasks = taskService.createTaskQuery().processInstanceIdIn(allProcessInstanceIds)
				.taskAssignee(userId).active().list();
		if (allTasks.size() > 1) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.TOO_MANY_TASKS);
		} else {
			if (allTasks.size() < 1) {
				response = new ApiResponse(errorcode, errorMessage);
			} else {
				/* can be child or can be main */
				// TODO: optimize this as much as possible
				ProcessInstance childProcessInstance = runtimeService.createProcessInstanceQuery()
						.processInstanceId(allTasks.get(0).getProcessInstanceId()).singleResult();
				HistoricProcessInstance parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
						.processInstanceId(parentProcessInstanceId).singleResult();
				Map<String, Object> processVariables = runtimeService.getVariables(childProcessInstance.getId());
				Map<String, Object> data = new HashMap<>();
				// checking if parent process instance Id is indeed parent
				// process instance
				if (parentProcessInstance.getSuperProcessInstanceId() != null) {
					parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(parentProcessInstance.getSuperProcessInstanceId()).singleResult();
				}
				data.put(Constants.LOAN_APPLICATION_ID_KEY, parentProcessInstance.getBusinessKey());
				LinkedHashSet<Map<String, String>> completedTasks = null;
				if (processVariables.get(Constants.COMPLETED_TASK_KEY) == null) {
					completedTasks = new LinkedHashSet<>();
				} else {
					completedTasks = (LinkedHashSet<Map<String, String>>) processVariables
							.get(Constants.COMPLETED_TASK_KEY);
				}
				data.put(Constants.COMPLETED_TASK_KEY, completedTasks);
				SimpleFormModel formModel = (SimpleFormModel) taskService.getTaskFormModel(allTasks.get(0).getId())
						.getFormModel();
				Map<String, FormField> formFields = formModel.allFieldsAsMap();
				response = HelperFunctions.makeResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE,
						parentProcessInstance.getId(), allTasks.get(0), data, formFields);
			}
		}
		return response;
	}

	public ApiResponse startOrResumeProcess(String userId, String journeyName) {
		ApiResponse response;
		ProcessInstance processInstance = null;
		try {
			try {
				CustomLogger.log(userId, null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null, null,
						journeyName);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null, null,
						e, journeyName);
			}
			// checking if any process is deployed by given journey name or not
			if (repositoryService.createDeploymentQuery().processDefinitionKey(journeyName).count() < 1) {
				// no process deployed with given journey name
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
						Constants.NO_PROCESS_DEPLOYED_FOR_GIVEN_KEY_MESSAGE);
			} else {
				// process is deployed by given journey name.
				// Checking for running process instances for current user for
				// given
				// journey name.
				List<ProcessInstance> processInstanceList = runtimeService.createProcessInstanceQuery()
						.processDefinitionKey(journeyName).active().startedBy(userId).includeProcessVariables()
						.orderByProcessInstanceId().desc().list();
				if (!processInstanceList.isEmpty()) {
					// process exist for user. So will be returning the active
					// task
					// (if any) for the given user.
					processInstance = processInstanceList.get(0);
					response = getTaskData(userId, processInstance.getId(), HttpStatus.BAD_REQUEST,
							Constants.NO_TASKS_MESSAGE);
				} else {
					// no process exist for user. So starting new process.
					Map<String, Object> processVariable = new HashMap<>();
					processVariable.put(Constants.PROCESS_TASKS_ASSIGNEE_KEY, userId);

					Authentication.setAuthenticatedUserId(userId);
					LOSApplicationNumber losApplicationNumber = new LOSApplicationNumber();
					losApplicationNumberDao.save(losApplicationNumber);
					// here setting LOS application number as business key for
					// process instance. It will help in searching process later
					// on if we have business key.
					processInstance = runtimeService.startProcessInstanceByKey(journeyName,
							Long.toString(losApplicationNumber.getId()), processVariable);
					losApplicationNumber.setProcessInstanceId(processInstance.getId());
					losApplicationNumberDao.save(losApplicationNumber);

					LOSUserModel user = losUserDao.findByIdAndIsDeleted(CommonHelperFunctions.getIdFromUserName(userId),
							false);
					if (user != null) {
						LOSUserLoanDataModel loan = new LOSUserLoanDataModel();
						loan.setProcessInstanceId(processInstance.getId());
						loan.setLosApplicationNumber(losApplicationNumber);
						loan.setLosUserModel(user);
						loan.setApplicationCompleted(false);
						loan.setApplicationStage(APPLICATION_STAGE.NEW.toString());
						losUserLoanDataDao.save(loan);
						response = getTaskData(userId, processInstance.getId(), HttpStatus.BAD_REQUEST,
								Constants.NO_TASKS_MESSAGE);
					} else {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE,
								Constants.INVALID_USER_ID_MESSAGE);
					}
				}
			}
			try {
				CustomLogger.log(userId, processInstance != null ? processInstance.getId() : null,
						LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
						processInstance != null ? processInstance.getBusinessKey() : null,
						processInstance != null ? processInstance.getBusinessKey() : null, response);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, processInstance != null ? processInstance.getId() : null,
						LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
						processInstance != null ? processInstance.getBusinessKey() : null,
						processInstance != null ? processInstance.getBusinessKey() : null, e, response);
			}
		} catch (Exception e) {
			CustomLogger.logException(userId, processInstance != null ? processInstance.getId() : null,
					LogType.EXCEPTION, JobType.INBOUND_API,
					processInstance != null ? processInstance.getBusinessKey() : null,
					processInstance != null ? processInstance.getBusinessKey() : null, e, journeyName);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	public ApiResponse getFormData(String userId, String parentProcessInstanceId) {
		ApiResponse response;
		try {
			try {
				CustomLogger.log(userId, parentProcessInstanceId, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
						null, null, parentProcessInstanceId);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, parentProcessInstanceId, LogType.INBOUND_REQUEST_BODY,
						JobType.INBOUND_API, null, null, e, "Process Instance Id : " + parentProcessInstanceId);
			}
			if (parentProcessInstanceId == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			} else {
				response = getTaskData(userId, parentProcessInstanceId, HttpStatus.BAD_REQUEST,
						Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			}
			try {
				CustomLogger.log(userId, parentProcessInstanceId, LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
						null, null, response);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, parentProcessInstanceId, LogType.INBOUND_RESPONSE_BODY,
						JobType.INBOUND_API, null, null, e, response);
			}
		} catch (Exception e) {
			CustomLogger.logException(userId, parentProcessInstanceId, LogType.EXCEPTION, JobType.INBOUND_API, null,
					null, e, parentProcessInstanceId);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	public ApiResponse submitFormData(String userId, SubmitFormClass input) {
		ApiResponse response;
		try {
			try {
				CustomLogger.log(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
						JobType.INBOUND_API, null, null, input);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
						JobType.INBOUND_API, null, null, e, input);
			}
			Task task = taskService.createTaskQuery().taskAssignee(userId).active().taskId(input.getTaskId())
					.singleResult();
			if (task == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
			} else {
				List<Execution> executionProcessInstance = runtimeService.createExecutionQuery()
						.processInstanceId(task.getProcessInstanceId()).list();
				if (!executionProcessInstance.get(0).getRootProcessInstanceId().equals(input.getProcessInstanceId())) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				} else {
					SimpleFormModel formModel = (SimpleFormModel) taskService.getTaskFormModel(task.getId())
							.getFormModel();
					List<FormField> listOfFormProp = formModel.getFields();
					Map<String, Object> formPropertiesMap = (!input.getFormProperties().isEmpty()
							? input.getFormProperties()
							: new HashMap<String, Object>());
					Map<Object, Object> validationResult = Validation.submitValidation(listOfFormProp,
							formPropertiesMap, true);
					if (!(boolean) validationResult.get("status")) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST,
								validationResult.get(Constants.MESSAGE_KEY).toString());
					} else {
						runtimeService.setVariables(task.getProcessInstanceId(), formPropertiesMap);
						taskService.complete(input.getTaskId());
						// String taskConfiguration = task.getDescription();
						// try {
						// JSONObject taskConfigurationObject = new JSONObject(taskConfiguration);
						// if (taskConfigurationObject.has(Constants.CAN_COMPLETE_TASK_KEY) &&
						// taskConfigurationObject
						// .get(Constants.CAN_COMPLETE_TASK_KEY) instanceof Boolean) {
						// if (taskConfigurationObject.getBoolean(Constants.CAN_COMPLETE_TASK_KEY)) {
						// taskService.complete(input.getTaskId());
						// } else {
						// logger.debug("Cannot Complete Task : " + task.getName());
						// }
						// }
						// } catch (JSONException e) {
						// logger.warn("Task Configuration for task : " + task.getName() + " is not a
						// valid JSON.");
						// }
						response = getTaskData(userId, input.getProcessInstanceId(), HttpStatus.OK,
								Constants.SUCCESS_MESSAGE);
					}
				}
			}
			try {
				CustomLogger.log(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
						JobType.INBOUND_API, null, null, response);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
						JobType.INBOUND_API, null, null, e, response);
			}
		} catch (Exception e) {
			CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.EXCEPTION, JobType.INBOUND_API,
					null, null, e, input);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;

	}

	@SuppressWarnings("unchecked")
	public ApiResponse changeProcessState(String userId, BackTaskInput input, BindingResult result) {
		ApiResponse response;
		try {
			try {
				CustomLogger.log(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
						JobType.INBOUND_API, null, null, input);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
						JobType.INBOUND_API, null, null, e, input);
			}
			HistoricProcessInstance processInstance = null;
			response = checkErrors(result);
			if (response == null) {
				List<String> allProcessInstanceIds = allProcessInstances(input.getProcessInstanceId());
				if (allProcessInstanceIds.size() < 1) {
					response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
				} else {
					Task task = taskService.createTaskQuery().taskId(input.getCurrentTaskId()).taskAssignee(userId)
							.active().singleResult();
					// Map<String, Object> processVariables =
					// runtimeService.getVariables(task.getProcessInstanceId());
					// LinkedHashSet<Map<String, String>> completedProcessTasks =
					// (LinkedHashSet<Map<String, String>>) processVariables
					// .get("completedProcessTasks");
					processInstance = historyService.createHistoricProcessInstanceQuery()
							.processInstanceId(task.getProcessInstanceId()).singleResult();
					List<HistoricTaskInstance> taskToGoTo = historyService.createHistoricTaskInstanceQuery()
							.processInstanceId(processInstance.getId())
							.taskDefinitionKey(input.getBackTaskDefinitionKey()).list();
					boolean flag = !taskToGoTo.isEmpty();
					if (!taskToGoTo.isEmpty()) {
						// Iterator<Map<String, String>> completedTasksIterator =
						// completedProcessTasks.iterator();
						// while (completedTasksIterator.hasNext()) {
						// Map<String, String> taskInfo = completedTasksIterator.next();
						// if
						// (taskInfo.get("taskDefinitionKey").equals(input.getBackTaskDefinitionKey())
						// && !flag) {
						// completedTasksIterator.remove();
						// flag = true;
						// } else if (flag) {
						// completedTasksIterator.remove();
						// }
						// }
						// if (flag) {
						// if
						// (input.getBackTaskDefinitionKey().equals(Constants.NO_OF_COBORROWERS_TASK_KEY))
						// {
						// runtimeService.removeVariable(processInstance.getId(),
						// Constants.COBORROWERS_LIST_KEY);
						// }
						// runtimeService.setVariable(processInstance.getId(),
						// Constants.COMPLETED_TASK_KEY,
						// completedProcessTasks);
						runtimeService.createChangeActivityStateBuilder().processInstanceId(task.getProcessInstanceId())
								.moveActivityIdTo(task.getTaskDefinitionKey(), input.getBackTaskDefinitionKey())
								.changeState();
						response = getTaskData(userId, input.getProcessInstanceId(), HttpStatus.BAD_REQUEST,
								Constants.INVALID_TASK_ID_MESSAGE);
						// }
					}
					if (taskToGoTo.isEmpty()) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.TASK_NOT_DONE_FOR_PROC_INST);
					} else if (!flag) {
						response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_BACK_TASK_DEFINITION_KEY);
					}
				}
			}
			try {
				CustomLogger.log(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
						JobType.INBOUND_API, null, null, response);
			} catch (JsonProcessingException e) {
				CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
						JobType.INBOUND_API, null, null, e, response);
			}
		} catch (Exception e) {
			CustomLogger.logException(userId, input.getProcessInstanceId(), LogType.EXCEPTION, JobType.INBOUND_API,
					null, null, e, input);
			response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, Constants.SOMETHING_WRONG_MESSAGE);
		}
		return response;
	}

	// public ApiResponse login(Map<String, Object> requestBody){
	// ApiResponse response;
	// String username = requestBody.get("username").toString();
	// String password = requestBody.get("password").toString();
	// boolean result = identityService.checkPassword(username, password);
	// if (!result) {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_CREDENTIALS);
	// return response;
	// }
	// String signature = username.concat(":").concat(password);
	// String token = Base64.getEncoder().encodeToString(signature.getBytes());
	// Map<String, Object> data = new HashMap<String, Object>();
	// data.put("token", token);
	// response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, data);
	// return response;
	//
	// }

	// @SuppressWarnings("unchecked")
	// public ApiResponse changeProcessStateNew(String userId, BackTaskInput
	// input, BindingResult result) {
	// ApiResponse response;
	// try {
	// try {
	// log(userId, input.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
	// JobType.INBOUND_API, null, null,
	// input);
	// } catch (JsonProcessingException e) {
	// logException(userId, input.getProcessInstanceId(),
	// LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
	// null, null, e, input);
	// }
	// response = checkErrors(result);
	// if (response == null) {
	// List<String> allProcessInstanceIds =
	// allProcessInstances(input.getProcessInstanceId());
	// if (allProcessInstanceIds.size() < 1) {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
	// } else {
	// Task task =
	// taskService.createTaskQuery().taskId(input.getCurrentTaskId()).taskAssignee(userId)
	// .active().singleResult();
	// if (task != null) {
	// String taskConfiguration = task.getDescription();
	// try {
	// JSONObject taskConfigurationObject = new JSONObject(taskConfiguration);
	// if (taskConfigurationObject.has(Constants.BACK_TASK_LIST_KEY)) {
	// if (taskConfigurationObject.get(Constants.BACK_TASK_LIST_KEY) instanceof
	// JSONArray) {
	// JSONArray backTaskArray = taskConfigurationObject
	// .getJSONArray(Constants.BACK_TASK_LIST_KEY);
	// List<Object> backTaskList = backTaskArray.toList();
	// if (backTaskList.contains(input.getBackTaskDefinitionKey())) {
	// runtimeService.createChangeActivityStateBuilder()
	// .processInstanceId(task.getProcessInstanceId())
	// .moveActivityIdTo(task.getTaskDefinitionKey(),
	// input.getBackTaskDefinitionKey())
	// .changeState();
	// response = getTaskData(userId, input.getProcessInstanceId(),
	// HttpStatus.BAD_REQUEST, Constants.INVALID_TASK_ID_MESSAGE);
	// } else {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_BACK_TASK_DEFINITION_KEY);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.BACK_TASK_LIST_NOT_ARRAY_MESSAGE);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.BACK_TASK_LIST_NOT_AVAILABLE_MESSAGE);
	// }
	// } catch (JSONException e) {
	// response = new ApiResponse(HttpStatus.EXPECTATION_FAILED,
	// Constants.TASK_CONFIGURATION_INVALID_JSON_MESSAGE);
	// }
	// } else {
	// response = new ApiResponse(HttpStatus.BAD_REQUEST,
	// Constants.INVALID_TASK_ID_MESSAGE);
	// }
	// }
	// }
	// try {
	// log(userId, input.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
	// JobType.INBOUND_API, "", "",
	// response);
	// } catch (JsonProcessingException e) {
	// logException(userId, input.getProcessInstanceId(),
	// LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
	// "", "", e, response);
	// }
	// } catch (Exception e) {
	// logException(userId, input.getProcessInstanceId(), LogType.EXCEPTION,
	// JobType.INBOUND_API, "", "", e,
	// input);
	// response = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR,
	// Constants.SOMETHING_WRONG_MESSAGE);
	// }
	// return response;
	// }

	/**
	 * This functions returns the access and refresh tokens for normal user on
	 * successful login.
	 * 
	 * @param customerLoginInput
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse loginUser(CustomerLoginInput customerLoginInput) {
		ApiResponse response = null;
		try {
			CustomLogger.log(customerLoginInput.getMobile(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API,
					null, null, customerLoginInput);
		} catch (JsonProcessingException e) {
			CustomLogger.logException(customerLoginInput.getMobile(), null, LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, e, customerLoginInput);
		}
		LOSUserModel user = losUserDao.findByContactNumberAndIsDeleted(customerLoginInput.getMobile(), false);
		if (user == null) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.FAILURE_MESSAGE);
		} else {
			String keycloakId = "user@" + user.getId() + ".com";
			String password = Base64.encodeBase64String(keycloakId.getBytes());
			response = keycloakManager.loginWithEmailAndPassword(keycloakId, password);
		}
		try {
			CustomLogger.log(customerLoginInput.getMobile(), null, LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API,
					null, null, response);
		} catch (JsonProcessingException e) {
			CustomLogger.logException(customerLoginInput.getMobile(), null, LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, e, response);
		}
		return response;
	}

	/**
	 * This functions copies all variables of root process to sub processes and vice
	 * versa.
	 * 
	 * @param execution
	 * @param copyToRoot
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution copyVariablesFromToRootProcess(Execution execution, boolean copyToRoot) {
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, "copyVariablesFromToRootProcess");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null,
					null, e, "copyVariablesFromToRootProcess");
		}
		String processInstanceId = execution.getProcessInstanceId();
		String rootProcessInstanceId = execution.getRootProcessInstanceId();
		if (processInstanceId != null && rootProcessInstanceId != null) {
			if (copyToRoot) {
				runtimeService.setVariables(rootProcessInstanceId, runtimeService.getVariables(processInstanceId));
			} else {
				runtimeService.setVariables(processInstanceId, runtimeService.getVariables(rootProcessInstanceId));
			}
		}
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, "copyVariablesFromToRootProcess");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), execution.getProcessInstanceId(),
					LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API, null, null, e,
					"copyVariablesFromToRootProcess");
		}
		return execution;
	}

	/**
	 * This functions that updates user basic information in table los_user from
	 * data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserData(Execution execution) {
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, "updateUserData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null,
					null, e, "updateUserData");
		}
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				if (processVariables.containsKey(Constants.BORROWER_FULL_NAME_KEY)) {
					user.setName(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.BORROWER_FULL_NAME_KEY)));
				}
				if (processVariables.containsKey(Constants.BORROWER_AGE_KEY)) {
					user.setAge(
							CommonHelperFunctions.getIntegerValue(processVariables.get(Constants.BORROWER_AGE_KEY)));
				}
				if (processVariables.containsKey(Constants.DATE_OF_BIRTH_KEY)) {
					user.setDateOfBirth(
							CommonHelperFunctions.getStringValue(processVariables.get(Constants.DATE_OF_BIRTH_KEY)));
				}
				if (processVariables.containsKey(Constants.GENDER_KEY)) {
					user.setSex(CommonHelperFunctions.getStringValue(processVariables.get(Constants.GENDER_KEY)));
				}
				if (processVariables.containsKey(Constants.EMAIL_ID_KEY)) {
					user.setEmail(CommonHelperFunctions.getStringValue(processVariables.get(Constants.EMAIL_ID_KEY)));
				}

				losUserDao.save(user);
			}
		}
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, "updateUserData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), execution.getProcessInstanceId(),
					LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API, null, null, e, "updateUserData");
		}
		return execution;
	}

	/**
	 * This functions that updates user loan information in table los_user_loan_data
	 * from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserLoanData(Execution execution) {
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, "updateUserLoanData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null,
					null, e, "updateUserLoanData");
		}
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		LOSUserLoanDataModel loan = losUserLoanDataDao.findByProcessInstanceIdAndIsDeleted(processInstanceId, false);
		if (loan != null) {
			if (processVariables.containsKey(Constants.LOAN_AMOUNT_KEY)) {
				loan.setLoanAmount(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.LOAN_AMOUNT_KEY)));
			}
			if (processVariables.containsKey(Constants.TENURE_KEY)) {
				loan.setTenure(CommonHelperFunctions.getIntegerValue(processVariables.get(Constants.TENURE_KEY)));
			}
			if (processVariables.containsKey(Constants.APPLICATION_START_DATE)) {
				loan.setApplicationDate(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.APPLICATION_START_DATE)));
			}
			if (processVariables.containsKey(Constants.LOAN_EMI_KEY)) {
				loan.setEmi(CommonHelperFunctions.getStringValue(processVariables.get(Constants.LOAN_EMI_KEY)));
			}
			if (processVariables.containsKey(Constants.DISBURSAL_DATE)) {
				loan.setDisbursementDate(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_DATE)));
			}
			if (processVariables.containsKey(Constants.DISBURSAL_CHANNEL)) {
				loan.setDisbursementChannel(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSAL_CHANNEL)));
			}
			if (processVariables.containsKey(Constants.DISBURSEMENT_STATUS_CODE)) {
				loan.setDisbursementStatus(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DISBURSEMENT_STATUS_CODE)));
			}
			losUserLoanDataDao.save(loan);
		}
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, "updateUserLoanData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), execution.getProcessInstanceId(),
					LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API, null, null, e, "updateUserLoanData");
		}
		return execution;
	}

	/**
	 * This functions that updates user address information in table
	 * los_user_address from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserAddressData(Execution execution) {
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, "updateUserAddressData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null,
					null, e, "updateUserAddressData");
		}
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				LOSUserAddressModel permanentAddress = losUserAddressDao.findByLosUserModelAndAddressType(user,
						Enums.AddressType.PERMANENT.toString());
				if (permanentAddress == null) {
					permanentAddress = new LOSUserAddressModel();
				}
				permanentAddress.setAddressLine1(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_ADDRESS_LINE_1_KEY)));
				permanentAddress.setAddressLine2(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_ADDRESS_LINE_2_KEY)));
				permanentAddress.setAddressType(Enums.AddressType.PERMANENT.toString());
				permanentAddress.setCityDistrict(CommonHelperFunctions
						.getStringValue(processVariables.get(Constants.PERMANENT_CITY_DISTRICT_KEY)));
				permanentAddress.setState(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.PERMANENT_PROVINCE_KEY)));
				permanentAddress.setLosUserModel(user);
				losUserAddressDao.save(permanentAddress);

				if (!processVariables.containsKey(Constants.TEMPORARY_ADDRESS_SAME_AS_PERMANENT_ADDRESS_KEY)
						|| !CommonHelperFunctions.getBooleanValue(
								processVariables.get(Constants.TEMPORARY_ADDRESS_SAME_AS_PERMANENT_ADDRESS_KEY))) {
					LOSUserAddressModel temporaryAddress = losUserAddressDao.findByLosUserModelAndAddressType(user,
							Enums.AddressType.CURRENT.toString());
					if (temporaryAddress == null) {
						temporaryAddress = new LOSUserAddressModel();
					}
					temporaryAddress.setAddressLine1(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_ADDRESS_LINE_1_KEY)));
					temporaryAddress.setAddressLine2(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_ADDRESS_LINE_2_KEY)));
					temporaryAddress.setAddressType(Enums.AddressType.CURRENT.toString());
					temporaryAddress.setCityDistrict(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_CITY_DISTRICT_KEY)));
					temporaryAddress.setState(CommonHelperFunctions
							.getStringValue(processVariables.get(Constants.TEMPORARY_PROVINCE_KEY)));
					temporaryAddress.setLosUserModel(user);
					losUserAddressDao.save(temporaryAddress);
				}

			}
		}
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, "updateUserAddressData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), execution.getProcessInstanceId(),
					LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API, null, null, e, "updateUserAddressData");
		}
		return execution;
	}

	/**
	 * This functions that updates user employment information in table
	 * los_user_employment from data present in process variables.
	 * 
	 * @param execution
	 * @return Execution
	 * 
	 * @author Kunal Arneja
	 */
	public Execution updateUserEmploymentData(Execution execution) {
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_REQUEST_BODY,
					JobType.INBOUND_API, null, null, "updateUserEmploymentData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), null, LogType.INBOUND_REQUEST_BODY, JobType.INBOUND_API, null,
					null, e, "updateUserEmploymentData");
		}
		String processInstanceId = execution.getProcessInstanceId();
		Map<String, Object> processVariables = runtimeService.getVariables(processInstanceId);
		Long userId = null;
		if (processVariables.containsKey(Constants.PROCESS_TASKS_ASSIGNEE_KEY)) {
			userId = CommonHelperFunctions.getIdFromUserName(
					CommonHelperFunctions.getStringValue(processVariables.get(Constants.PROCESS_TASKS_ASSIGNEE_KEY)));
			LOSUserModel user = losUserDao.findByIdAndIsDeleted(userId, false);
			if (user != null) {
				LOSUserEmploymentModel employmentDetails = losUserEmploymentDao.findByLosUserModel(user);
				if (employmentDetails == null) {
					employmentDetails = new LOSUserEmploymentModel();
				}
				employmentDetails.setCompanyName(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.COMPANY_NAME_KEY)));
				employmentDetails.setCompanyType(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.COMPANY_TYPE_KEY)));
				employmentDetails
						.setEmploymentStatus(CommonHelperFunctions.getStringValue(Enums.EmploymentStatus.SALARIED));
				employmentDetails.setOccupation(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.OCCUPATION_KEY)));
				employmentDetails.setPosition(
						CommonHelperFunctions.getStringValue(processVariables.get(Constants.DESIGNATION_KEY)));
				employmentDetails.setLosUserModel(user);
				losUserEmploymentDao.save(employmentDetails);
			}
		}
		try {
			CustomLogger.log(execution.getId(), execution.getProcessInstanceId(), LogType.INBOUND_RESPONSE_BODY,
					JobType.INBOUND_API, null, null, "updateUserEmploymentData");
		} catch (JsonProcessingException e) {
			CustomLogger.logException(execution.getId(), execution.getProcessInstanceId(),
					LogType.INBOUND_RESPONSE_BODY, JobType.INBOUND_API, null, null, e, "updateUserEmploymentData");
		}
		return execution;
	}

}
