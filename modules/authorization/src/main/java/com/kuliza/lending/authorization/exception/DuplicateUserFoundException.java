package com.kuliza.lending.authorization.exception;

public class DuplicateUserFoundException extends RuntimeException {

	private static final long serialVersionUID = 6364887010663092744L;

	public DuplicateUserFoundException(String message) {
		super(message);
	}
}
