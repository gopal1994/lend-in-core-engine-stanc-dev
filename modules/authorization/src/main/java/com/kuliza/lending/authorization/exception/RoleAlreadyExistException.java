package com.kuliza.lending.authorization.exception;

public class RoleAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = 9187913733213733305L;

	public RoleAlreadyExistException(String message) {
		super(message);
	}

}
