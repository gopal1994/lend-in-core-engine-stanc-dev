package com.kuliza.lending.authorization.exception;

public class RoleDoesNotExistException extends RuntimeException {

	private static final long serialVersionUID = 3017115273241318921L;

	public RoleDoesNotExistException(String message) {
		super(message);
	}

}
