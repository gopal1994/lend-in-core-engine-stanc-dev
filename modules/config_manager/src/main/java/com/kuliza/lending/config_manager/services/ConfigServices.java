package com.kuliza.lending.config_manager.services;

import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.config_manager.dao.ConfigsDao;
import com.kuliza.lending.config_manager.models.ConfigModel;
import com.kuliza.lending.config_manager.pojo.ConfigInput;

@Service
public class ConfigServices {
	
	private static final Logger logger=LoggerFactory.getLogger(ConfigServices.class);

	@Autowired
	ConfigsDao configsDao;

	/**
	 * This function returns the map with respect to a client ID.
	 * 
	 * @param ConfigInput
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse getConfigs(String clientId) {
		logger.info("--> Entering Get configuration for Client() :"+clientId);
		ConfigModel configModel = configsDao.findByClientId(clientId);
		if (configModel != null) {
			JSONObject jsonObj = new JSONObject(configModel);
			if (jsonObj.has("jsonString") && jsonObj.get("jsonString") instanceof String) {
				String jsonString = jsonObj.getString("jsonString");
				Map<String, Object> responseMap = CommonHelperFunctions.jsonStringToMap(jsonString);
				logger.info("<--Exiting get configurtion()");
				return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, responseMap);
			}
		}
		logger.error("Invalid client id : "+clientId);
		return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_CLIENT_ID_MESSAGE);
	}

	/**
	 * This function sets the jsonString with respect to a client ID.
	 * 
	 * @param ConfigInput
	 * @return ApiResponse
	 * 
	 * @author Kunal Arneja
	 */
	public ApiResponse setConfig(ConfigInput input) {
		logger.info("--> Entering set Configuration for Client()");
		if (CommonHelperFunctions.isJSONValid(input.getJsonString())) {
			ConfigModel oldConfig = configsDao.findByClientId(input.getClientId());
			if (oldConfig != null) {
				logger.warn("Deleting Existing Config for client: "+input.getClientId());
				configsDao.delete(oldConfig);
				logger.debug("SuccessFully Deleted existing configuration");
			}
			ConfigModel newConfig = new ConfigModel();
			newConfig.setClientId(input.getClientId());
			newConfig.setJsonString(input.getJsonString());
			configsDao.save(newConfig);
			logger.info("<--Successfully Saved Config Exiting set config() ");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} else {
			logger.error("Invalid JSON Configuration for client");
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_JSON_CONFIG_MESSAGE);
		}

	}

}
