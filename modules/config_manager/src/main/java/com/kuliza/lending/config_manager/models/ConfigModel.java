package com.kuliza.lending.config_manager.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
@Table(name="config_module")
public class ConfigModel extends BaseModel{
	
	@NotNull
	@Column(unique=true)
	String clientId;
	
	@NotNull
	@Lob
	String jsonString;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public ConfigModel() {
		super();
		this.setIsDeleted(false);
	}

	public ConfigModel(@NotNull String clientId, @NotNull String jsonString) {
		super();
		this.setIsDeleted(false);
		this.clientId = clientId;
		this.jsonString = jsonString;
	}

	
}
