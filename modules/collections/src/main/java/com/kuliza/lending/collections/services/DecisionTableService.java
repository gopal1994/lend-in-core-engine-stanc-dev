package com.kuliza.lending.collections.services;

import java.util.Map;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.collections.pojo.DecisionTableDefinitionRepresentation;
import com.kuliza.lending.collections.model.DecisionTableModel;
import com.kuliza.lending.collections.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.collections.repository.DecisionTableRepository;
import org.flowable.dmn.api.DmnRepositoryService;
import org.flowable.dmn.api.DmnRuleService;
import org.flowable.dmn.model.DmnDefinition;
import org.flowable.editor.dmn.converter.DmnJsonConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class DecisionTableService {

    @Autowired
    private DecisionTableRepository decisionTableRepository;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    private DmnRepositoryService dmnRepositoryService;

    @Autowired
    private DmnRuleService dmnRuleService;


    // This is function to save decision table to database using repository function
    public DecisionTableModel addDecisionTable(DecisionTableModelRepresentation decisionTableModelRepresentation) throws Exception {
        return decisionTableRepository.save(checkAndSetDecisionModel(decisionTableModelRepresentation));

    }

    // This function deploys the provided decision table model and returns the deployed model, throws exception if any
    public DecisionTableModel deployDecisionTable(String decisionKey, Integer version) throws Exception{
        DecisionTableModel savedModel = fetchDecisionTable(decisionKey, version) ;
        if(savedModel==null)
            throw new Exception("Unable to fetch decision for the provided key and version");

        try {
            JsonNode decisionTableNode = objectMapper.readTree(savedModel.getModelEditorJson());
            DmnDefinition dmnDefinition = new DmnJsonConverter().convertToDmn(decisionTableNode,String.valueOf(savedModel.getId()),
                    savedModel.getVersion(), savedModel.getModified());
            String uniqueName = savedModel.getNameKey() + "_" + savedModel.getVersion();
            dmnRepositoryService.createDeployment().name("deployment_"+uniqueName)
                    .addDmnModel("dmn_" + uniqueName + ".dmn", dmnDefinition).deploy();

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return savedModel;

    }

    // This function executes the input against the decision table identified by the provided decision table key
    public Map<String, Object> executeDecisionTable(String decisionKey, Map<String, Object> inputVariables){
        return  dmnRuleService.createExecuteDecisionBuilder().decisionKey(decisionKey).variables(inputVariables)
                .executeWithSingleResult();
    }

    // The function soft deletes he decision table in the database
    public DecisionTableModel deleteDecisionTable(String decisionKey, Integer version) throws Exception
    {
        DecisionTableModel decisionTableModel = fetchDecisionTable(decisionKey, version);
        if(decisionTableModel==null)
            throw new Exception("Decision table for this key and version not found");
        decisionTableModel.setIsDeleted(true);
        return  decisionTableRepository.save(decisionTableModel);

    }

    // This function returns the DecisionTableModelRepresentation of the decision table with provided parameters
    public DecisionTableModelRepresentation fetchDecisionTableDefinition(String decisionKey, Integer version) throws Exception {
        DecisionTableModel savedModel = fetchDecisionTable(decisionKey, version) ;
        if(savedModel==null)
            throw  new Exception("Decision table for this key and version not found");
        DecisionTableDefinitionRepresentation decisionTableDefinitionRepresentation = null;
        try {
            decisionTableDefinitionRepresentation = objectMapper.readValue(savedModel.getModelEditorJson(), DecisionTableDefinitionRepresentation.class);
        } catch (Exception e) {
            throw new Exception("Could not deserialize decision table definition");
        }
        DecisionTableModelRepresentation decisionTableModelRepresentation = new DecisionTableModelRepresentation(savedModel);
        decisionTableModelRepresentation.setDecisionTableDefinition(decisionTableDefinitionRepresentation);
        return decisionTableModelRepresentation;
    }

    // This function creates or updates an entry in the decision table model
    private DecisionTableModel checkAndSetDecisionModel(DecisionTableModelRepresentation decisionTableModelRepresentation) throws Exception{
        DecisionTableModel previousModel = decisionTableRepository.findFirstByNameKeyOrderByVersionDesc(decisionTableModelRepresentation.getKey());
        DecisionTableModel currentModel ;
        // Enters this if only if there is an existing entry for the same key
        if(previousModel!=null)
        {
            // Checks whether user wants to update the latest version of the existing model
            if (decisionTableModelRepresentation.isNewVersion() == false){
                currentModel = previousModel;
                currentModel.setName(decisionTableModelRepresentation.getName());
                currentModel.setComment(decisionTableModelRepresentation.getComment());
                currentModel.setDescription(decisionTableModelRepresentation.getDescription());

            }
            // Else create a new version of the decision table
            else{
                currentModel = new DecisionTableModel(decisionTableModelRepresentation) ;
                currentModel.setVersion(previousModel.getVersion() + 1);
            }

        }
        // Else create an entry with new key
        else{
            currentModel = new DecisionTableModel(decisionTableModelRepresentation) ;
            currentModel.setVersion(1);
        }
        decisionTableModelRepresentation.getDecisionTableDefinition().setName(decisionTableModelRepresentation.getName());
        decisionTableModelRepresentation.getDecisionTableDefinition().setKey(decisionTableModelRepresentation.getKey());
        decisionTableModelRepresentation.getDecisionTableDefinition().setId(decisionTableModelRepresentation.getKey() + "_" + currentModel.getVersion());
        String editorJson = null;
        try {
            editorJson = objectMapper.writeValueAsString(decisionTableModelRepresentation.getDecisionTableDefinition());
            currentModel.setModelEditorJson(editorJson);
        } catch (Exception e) {
            throw new Exception(e.getMessage());

        }
        return currentModel;
    }

    // This function returns the decision table with the provided decisionKey, if version is not provided it returns latest version
    private DecisionTableModel fetchDecisionTable(String decisionKey, Integer version)  {
        DecisionTableModel decisionModel;
        if (version == null) {
            decisionModel = decisionTableRepository.findFirstByNameKeyAndIsDeletedOrderByVersionDesc(decisionKey, false);
        }
        else
        {
            decisionModel = decisionTableRepository.findByNameKeyAndVersionAndIsDeleted(decisionKey, version, false);
        }
        return decisionModel;
    }



}
