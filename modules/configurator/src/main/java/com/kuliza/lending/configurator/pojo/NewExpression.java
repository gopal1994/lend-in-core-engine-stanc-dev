package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewExpression {

	@NotNull(message = "expressionString is a required Key")
	@Size(min = Constants.MIN_LENGTH_EXPRESSION, max = Constants.MAX_LENGTH_EXPRESSION, message = "Expression String length must be greater than 0 and less than 10000")
	private String expressionString;

	private String productId;
	private String groupId;
	private String expressionId;

	public NewExpression() {
		this.expressionString = "";
	}

	public NewExpression(String expressionString) {
		this.expressionString = expressionString;
	}

	public String getExpressionString() {
		return expressionString;
	}

	public void setExpressionString(String expressionString) {
		this.expressionString = expressionString;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getExpressionId() {
		return expressionId;
	}

	public void setExpressionId(String expressionId) {
		this.expressionId = expressionId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("expressionString : " + expressionString);
	// inputData.append(" }");
	// return inputData.toString();
	// }

}
