package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SubmitNewUser {

	@NotNull(message = "name is a required key")
	@NotEmpty(message = "Name cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "Name can be of max length 255")
	String name;
	@NotNull(message = "password is a required key")
	@NotEmpty(message = "Password cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "Password can be of max length 255")
	String password;
	@NotNull(message = "companyName is a required key")
	@NotEmpty(message = "Company Name cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "Company Name can be of max length 255")
	String companyName;
	@NotNull(message = "userName is a required key")
	@NotEmpty(message = "User Name cannot be Empty")
	@Size(max = Constants.MAX_LENGTH_STRING, message = "User Name can be of max length 255")
	String userName;

	public SubmitNewUser() {
		this.name = "";
		this.password = "";
		this.companyName = "";
		this.userName = "";
	}

	public SubmitNewUser(String name, String password, String companyName, String userName) {
		this.name = name;
		this.password = password;
		this.companyName = companyName;
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("name : " + name + ", ");
	// inputData.append("password : " + password + ", ");
	// inputData.append("companyName : " + companyName + ", ");
	// inputData.append("userName : " + userName);
	// inputData.append(" }");
	// return inputData.toString();
	//
	// }

}
