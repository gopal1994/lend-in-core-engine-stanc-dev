package com.kuliza.lending.configurator.service.genericservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.configurator.CreditEngineApplication;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
public class GenericServicesCE {

	@Autowired
	private ObjectMapper objectMapper;

	private static final Logger logger = LoggerFactory.getLogger(CreditEngineApplication.class);

	public GenericAPIResponse checkErrors(BindingResult result) throws Exception {
		GenericAPIResponse response = null;
		if (result.hasErrors()) {
			Map<String, Object> data = new HashMap<>();
			data.put(Constants.DATA_ERRORS_KEY, HelperFunctions.generateErrorResponseData(result.getFieldErrors()));
			response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE, Constants.FAILURE_MESSAGE,
					data);
		}
		return response;
	}

	public GenericAPIResponse handleException(Exception e, HttpServletRequest request, String userId) {
		if (logger.isErrorEnabled()) {
			logger.error(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
					userId, HelperFunctions.getStackTrace(e)));
		}
		return new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INTERNAL_SERVER_ERROR_MESSAGE);
	}

	public GenericAPIResponse handleException(Exception e, HttpServletRequest request, String userId, Object input) {
		if (logger.isErrorEnabled()) {
			try {
				logger.error(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
						userId, HelperFunctions.generateLogData(objectMapper.writeValueAsString(input),
								HelperFunctions.getStackTrace(e))));
			} catch (Exception jsonParseException) {
				logger.error(HelperFunctions.getStackTrace(jsonParseException));
				return new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INTERNAL_SERVER_ERROR_MESSAGE);
			}
		}
		return new GenericAPIResponse(Constants.INTERNAL_SERVER_ERROR_CODE, Constants.FAILURE_MESSAGE,
				Constants.INTERNAL_SERVER_ERROR_MESSAGE);
	}

	public void logSuccessResponse(HttpServletRequest request, String userId, GenericAPIResponse response)
			throws JsonProcessingException {
		if (logger.isInfoEnabled()) {
			logger.info(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
					userId, objectMapper.writeValueAsString(response)));
		}
	}

	public void logErrorResponse(HttpServletRequest request, String userId, GenericAPIResponse response)
			throws JsonProcessingException {
		if (logger.isErrorEnabled()) {
			logger.error(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
					userId, objectMapper.writeValueAsString(response)));
		}
	}

	public void logSuccessResponse(HttpServletRequest request, String userId, GenericAPIResponse response, Object input)
			throws JsonProcessingException {
		if (logger.isInfoEnabled()) {
			logger.info(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
					userId, HelperFunctions.generateLogData(objectMapper.writeValueAsString(input),
							objectMapper.writeValueAsString(response))));
		}
	}

	public void logErrorResponse(HttpServletRequest request, String userId, GenericAPIResponse response, Object input)
			throws JsonProcessingException {
		if (logger.isErrorEnabled()) {
			logger.error(HelperFunctions.generateLogString(request.getMethod(), request.getRequestURL().toString(),
					userId, HelperFunctions.generateLogData(objectMapper.writeValueAsString(input),
							objectMapper.writeValueAsString(response))));
		}
	}

}
