package com.kuliza.lending.configurator.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OrderBy;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class BaseModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@OrderBy(clause = "id ASC")
	private long id;

	private Date created;

	private Date modified;

	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private Boolean isDeleted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonIgnore
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@JsonIgnore
	public Date getCreated() {
		return created == null ? null : (Date) created.clone();
	}

	public void setCreated(Date created) {
		this.created = created == null ? null : (Date) created.clone();
	}

	@JsonIgnore
	public Date getModified() {
		return modified == null ? null : (Date) modified.clone();
	}

	public void setModified(Date modified) {
		this.modified = modified == null ? null : (Date) modified.clone();
	}

}
